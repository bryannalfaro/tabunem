package sample;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Preguntas {
    public void changeScreenButtonPushed(javafx.event.ActionEvent actionEvent) throws IOException {
        Parent mitosViewParent = FXMLLoader.load(getClass().getResource("sample.fxml"));
        Scene mitosViewScene = new Scene(mitosViewParent);

        Stage window = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        window.setScene(mitosViewScene);
        window.show();
    }
}
